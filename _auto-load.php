<?php
function autoLoader($classname) {
    $filename = $classname . '.php';
    if (file_exists($filename)) {
        require_once($filename);
    }
}
spl_autoload_register('autoLoader');

?>