<?php
class Curl
{
    public function curlUrl($url) {
        $curlSession = $this->newCurlSession($url);
        $ch = $curlSession['curlRsource'];
        $proxy = $curlSession['proxy'];
        $htmdata = curl_exec($ch);

        $this->newCurlSession($url);
        $htmdata = curl_exec($ch);
        if (!$htmdata) {
            $error = curl_error($ch);
        }
        curl_close($ch);
        return $htmdata;
    }
    public function newCurlSession($url) {
        $proxy = '';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        return array('curlRsource' => $ch, 'proxy' => $proxy);
    }
}