<?php
/**
 * Created by PhpStorm.
 * User: shubham
 * Date: 2/2/19
 * Time: 5:22 PM
 */

class Scrapper
{
    public function getAttrData($attr, DomDocument $dom)
    {
        $attrData = array();
        $questionCounter = 0;
        //Loop through each tag in the dom and add it's attribute data to the array
        foreach ($dom->getElementsByTagName('*') as  $tag) {
            // qstn-row is category class..
            if (empty($tag->getAttribute($attr)) === false && $tag->getAttribute($attr) == 'qstn-row') {
                $questionCounter = $questionCounter + 1;
                $items= $tag->getElementsByTagName('a');
                $attrData[$questionCounter]['category'] = $items->item(1)->nodeValue ? $items->item(1)->nodeValue : $items->item(0)->nodeValue;
            }

            // dtl-qstn class of question text contain
            if (empty($tag->getAttribute($attr)) === false && $tag->getAttribute($attr) == 'dtl-qstn') {
                $attrData[$questionCounter]['question_text'] = ($tag->nodeValue);
            }
            // inf-block it contains Answer Data by User
            if (empty($tag->getAttribute($attr)) === false && $tag->getAttribute($attr) == 'inf-block') {
                $items= $tag->getElementsByTagName('a');
                $attrData[$questionCounter]['user_name'] = $items->item(0)->firstChild->nodeValue;

                $items= $tag->getElementsByTagName('p');
                $attrData[$questionCounter]['user_level'] = $items->item(0)->nodeValue;

                $items= $tag->getElementsByTagName('div');
                $attrData[$questionCounter]['answer_text'] = $this->filterText($items->item(0)->nodeValue);

            }
        }
        return ($attrData);
    }
    /*
     * Filter Url from Text and return filter string.
     */
    public function filterText($text){
        $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
        if(preg_match($reg_exUrl, $text, $url)) {
            return trim(preg_replace($reg_exUrl, "<a href=".$url[0].">$url[0]</a> ", $text));
        } else {
            return trim($text);
        }
    }

}