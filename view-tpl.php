<!DOCTYPE html>
<html lang="en">
<head>
    <title>Crawling</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">

    <table class="table">
        <thead>
        <tr>
            <th>Category</th>
            <th>Question</th>
            <th>Answer</th>
            <th>Added by</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $key => $value){ ?>
            <tr>
                <td width="10%"><?php echo $value['category'] ?></td>
                <td width="30%"><?php echo $value['question_text'] ?></td>
                <td width="40%"><?php echo $value['answer_text']? $value['answer_text']:'N/A' ?></td>
                <td width="20%"><strong>User Name:- </strong><?php echo $value['user_name'] ? $value['user_name'] : 'N/A' ?><br><?php echo $value['user_level'];?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

</body>
</html>
